# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Frank B. Brokken's C++ classes"
HOMEPAGE="https://gitlab.com/fbb-git/bobcat"
SRC_URI="https://gitlab.com/fbb-git/bobcat/-/archive/5.09.01/bobcat-5.09.01.tar.gz"

LICENSE="GPLv3"
SLOT="0"
KEYWORDS="amd64"

DEPEND="dev-util/icmake"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	unpack ${A}
	mv -v bobcat-5.09.01/bobcat libbobcat-5.09.01
	rm -rf bobcat-5.09.01
}

src_compile() {
	./build libraries light
}

src_install() {
	./build install x ${D}
}
