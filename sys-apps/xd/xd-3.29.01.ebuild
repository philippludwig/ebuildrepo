# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Frank B. Brokken's xd"
HOMEPAGE="https://gitlab.com/fbb-git/xd"
SRC_URI="https://gitlab.com/fbb-git/xd/-/archive/3.29.01/xd-3.29.01.tar.gz"

LICENSE="GPLv3"
SLOT="0"
KEYWORDS="amd64"

DEPEND="dev-cpp/libbobcat"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	unpack ${A}
	mv -v xd-3.29.01/xd xd
	rm -rf xd-3.29.01
	mv -v xd xd-3.29.01
}

src_compile() {
	./build program
}

src_install() {
	./build install b ${D}
}
